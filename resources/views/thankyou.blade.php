@extends('layout')

@section('title', 'Thank You')


@section('extra-css')
    <link rel="stylesheet" href="{{ asset('css/algolia.css') }}">
@endsection

@section('body-class', 'sticky-footer')

@section('content')

<!-- catg header banner section -->
<section id="aa-catg-head-banner">
    <img src="{{ asset('img/fashion/fashion-header-bg-8.jpg') }}" alt="fashion img">
    <div class="aa-catg-head-banner-area">
        <div class="container">
            <div class="aa-catg-head-banner-content">
                <h2>Thank You</h2>
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <!--<li><i class="fa fa-chevron-right breadcrumb-separator"></i></li>  -->       
                    <li class="">Shop</li>
                    <li class="">Checkout</li>
                    <li class="active">Thank You</li>
                </ol>
            </div>
        </div>
    </div>
</section>  
<!-- / catg header banner section -->


<section id="aa-product-category">
    <!-- notification section -->
    <br>
    <div class="container">
    @if (session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @endif

    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    </div>
    <!-- / notification section -->

</section>

<!-- Login View section -->
 <section id="aa-myaccount">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="aa-myaccount-area thank-you-section">         
      		<h1>Thank you for <br> Your Order!</h1>
       		<p>A confirmation email was sent</p>
       		<br>
       		<br>
       		<div>
           		<a href="{{ url('/') }}" class="aa-browse-btn">Home Page</a>
       		</div>
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Login View section -->





@endsection
