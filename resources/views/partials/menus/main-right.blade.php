<ul class="aa-head-top-nav-right">
    
    <li class="hidden-xs"><a href="{{ route('cart.index') }}">My Cart</a></li>
    <li class="hidden-xs"><a href="{{ route('checkout.index') }}">Checkout</a></li>
    @guest
    <li class="hidden-xs"><a href="{{ route('register') }}">Sign Up</a></li>
    <li class="hidden-xs"><a href="{{ route('login') }}">Login</a></li>
    @else
    <li><a href="{{ route('users.edit') }}">My Account</a></li>
    <li>
        <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
            Logout
        </a>
    </li>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
    @endguest

</ul>
