<ul>
    @foreach($items as $menu_item)
        <a href="{{ $menu_item->link() }}"><span class="fa {{ $menu_item->title }}"></span></a>
    @endforeach
</ul>
