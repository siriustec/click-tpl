<div class="aa-cartbox">
	<a class="aa-cart-link" href="{{ route('cart.index') }}">
	<span class="fa fa-shopping-basket"></span>
	<span class="aa-cart-title">SHOPPING CART</span>
	@if (Cart::instance('default')->count() > 0)
	<span class="aa-cart-notify">{{ Cart::instance('default')->count() }}</span>
	@endif
	</a>
	<div class="aa-cartbox-summary">
		@if (Cart::instance('default')->count() > 0)
		<ul>
			@foreach (Cart::content() as $item)
			<li>
				<a class="aa-cartbox-img" href="#">
				<img src="{{ productImage($item->model->image) }}" alt="img">
				</a>
				<div class="aa-cartbox-info">
					<h4><a href="#">{{ $item->model->name }}</a></h4>
					<p>{{ $item->qty }} x {{ presentPrice($item->price) }}</p>
				</div>
				<a class="aa-remove-product" href="#"><span class="fa fa-times"></span></a>
				<form action="{{ route('cart.destroy', $item->rowId) }}" id="deleteFromCart_{{ $item->slug}}" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
					<a class="aa-remove-product" href="#" onclick="document.getElementById('deleteFromCart_{{ $item->slug}}').submit();"><span class="fa fa-times"></span></a>
				</form>
			</li>
			@endforeach
			<li>
				<span class="aa-cartbox-total-title">
				Tax
				</span>
				<span class="aa-cartbox-total-price">
				{{ isset($newTax) ? presentPrice($newTax) : '' }} <br>
				</span>
			</li>
			<li>
				<span class="aa-cartbox-total-title">
				Total
				</span>
				<span class="aa-cartbox-total-price">
				{{  isset($newTotal) ? presentPrice($newTotal) : ''}}
				</span>
			</li>
		</ul>
		<a class="aa-cartbox-checkout aa-primary-btn" href="{{ route('checkout.index') }}">Checkout</a>
		@else
		<span class="aa-cartbox-total-price">
			No Items in cart
		</span>
		@endif
	</div>
</div>