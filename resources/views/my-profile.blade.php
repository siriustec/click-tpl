@extends('layout')

@section('title', 'My Profile')

@section('extra-css')
    <link rel="stylesheet" href="{{ asset('css/algolia.css') }}">
@endsection

@section('content')

<!-- catg header banner section -->
<section id="aa-catg-head-banner">
    <img src="{{ asset('img/fashion/fashion-header-bg-8.jpg') }}" alt="fashion img">
    <div class="aa-catg-head-banner-area">
        <div class="container">
            <div class="aa-catg-head-banner-content">
                <h2>My Profile</h2>
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <!--<li><i class="fa fa-chevron-right breadcrumb-separator"></i></li>  -->       
                    <li class="active">My Profile</li>
                </ol>
            </div>
        </div>
    </div>
</section>  
<!-- / catg header banner section -->

<section id="aa-product-category">
    <!-- notification section -->
    <br>
    <div class="container">
    @if (session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @endif

    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    </div>
    <!-- / notification section -->
</section>


 <!-- Login View section -->
 <section id="aa-myaccount">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="aa-myaccount-area">         
            <div class="row">

              <!-- Login further links -->
              <div class="col-lg-9 col-md-9 col-sm-8 col-md-push-3">
                <div class="aa-myaccount-login">
                <h4>My Profile</h4>

                <form action="{{ route('users.update') }}" method="POST" class="aa-login-form">
                    @method('patch')
                    @csrf
                    <label for="">Name</label>
                    <input id="name" type="text" name="name" value="{{ old('name', $user->name) }}" placeholder="Name" required>
                    
                    <label for="">Email</label>
                    <input id="email" type="text" name="email" value="{{ old('email', $user->email) }}" placeholder="Email" required>

                    <label for="">Password <span>Leave password blank to keep current password</span></label>
                    <input type="password" id="password" name="password" value="{{ old('password') }}" placeholder="Password" required>
                    <input id="password-confirm" type="password" name="password_confirmation" placeholder="Confirm Password">

                    <button type="submit" class="aa-browse-btn">Update Profile</button>
                  </form>
                </div>
              </div>
              <!-- / Login further links -->
              <!-- Login further links -->
              <div class="col-lg-3 col-md-3 col-sm-4 col-md-pull-9">
                    <aside class="aa-sidebar">
                        <!-- single sidebar -->
                        <div class="aa-sidebar-widget">
                            <h3>Profile</h3>
                            <ul class="aa-catg-nav">
                                <li class="active">
                                    <a href="{{ route('users.edit') }}">
                                        My Profile
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('orders.index') }}" >
                                        My Orders
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </aside>
                </div>
              <!-- / Login further links -->

            </div>          
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Login View section -->



@endsection

@section('extra-js')
    <!-- Include AlgoliaSearch JS Client and autocomplete.js library -->
    <script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
    <script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.min.js"></script>
    <script src="{{ asset('js/algolia.js') }}"></script>
@endsection
