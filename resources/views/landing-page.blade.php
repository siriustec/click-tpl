<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Shop | Sirius Click</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- Styles 
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">-->

        <!-- Font awesome -->
        <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">   
        <!-- SmartMenus jQuery Bootstrap Addon CSS -->
        <link href="{{ asset('css/jquery.smartmenus.bootstrap.css') }}" rel="stylesheet">
        <!-- Product view slider -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.simpleLens.css') }}">    
        <!-- slick slider -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css') }}">
        <!-- price picker slider -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/nouislider.css') }}">
        <!-- Theme color -->
        <link id="switcher" href="{{ asset('css/theme-color/siriusclick-theme.css') }}" rel="stylesheet">
        <!-- <link id="switcher" href="css/theme-color/bridge-theme.css" rel="stylesheet"> -->
        <!-- Top Slider CSS -->
        <link href="{{ asset('css/sequence-theme.modern-slide-in.css') }}" rel="stylesheet" media="all">

        <!-- Main style sheet -->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">    

        <!-- Google Font -->
        <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('css/algolia.css') }}">
    
        @yield('extra-css')

    </head>

    <body> 
   <!-- wpf loader Two -->
    <div id="wpf-loader-two">          
      <div class="wpf-loader-two-inner">
        <span>Loading</span>
      </div>
    </div> 
    <!-- / wpf loader Two -->       
  <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


    @include('partials.menus.main')


  <!-- logo section -->
  @include('landing.logo')
  <!-- / logo section -->

  <!-- Hero Slider section -->
  @include('landing.heroSlider')
  <!-- / Hero Slider section -->

 
  <!-- Start Promo section -->
  @include('landing.promoGrid')
  <!-- / Promo section -->


  <!-- Products section -->
     @include('landing.products')
  <!-- / Products section -->

  <!-- banner section -->
     @include('landing.banner')
  <!-- / banner section -->

  <!-- popular section -->
     @include('landing.popular')
  <!-- / popular section -->

  <!-- Support section -->
     @include('landing.support')
  <!-- / Support section -->

  <!-- Testimonial -->
    {{-- @include('landing.testimonial') --}}
  <!-- / Testimonial -->

  <!-- Blog -->
    {{-- @include('landing.blog') --}}
  <!-- / Blog -->

  <!-- Client Brand -->
    {{-- @include('landing.brand') --}}
  <!-- / Client Brand -->

  <!-- Subscribe section -->
     @include('landing.subscribe')
  <!-- / Subscribe section -->

  <!-- footer -->  
    @include('partials.footer')
  <!-- / footer -->



  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.js"></script>  
  <!-- SmartMenus jQuery plugin -->
  <script type="text/javascript" src="js/jquery.smartmenus.js"></script>
  <!-- SmartMenus jQuery Bootstrap Addon -->
  <script type="text/javascript" src="js/jquery.smartmenus.bootstrap.js"></script>  
  <!-- To Slider JS -->
  <script src="js/sequence.js"></script>
  <script src="js/sequence-theme.modern-slide-in.js"></script>  
  <!-- Product view slider -->
  <script type="text/javascript" src="js/jquery.simpleGallery.js"></script>
  <script type="text/javascript" src="js/jquery.simpleLens.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="js/nouislider.js"></script>
  <!-- Custom js -->
  <script type="text/javascript">
            var loadingGif = '{{ URL::asset('img/view-slider/loading.gif') }}'; 
  </script>
  <script src="js/custom.js"></script>    
</html>
