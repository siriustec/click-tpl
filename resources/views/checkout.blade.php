@extends('layout')

@section('title', 'Checkout')

@section('extra-css')
    <style>
        .mt-32 {
            margin-top: 32px;
        }
    </style>

    <script src="https://js.stripe.com/v3/"></script>

    <link rel="stylesheet" href="{{ asset('css/algolia.css') }}">

@endsection

@section('content')


<!-- catg header banner section -->
<section id="aa-catg-head-banner">
    <img src="{{ asset('img/fashion/fashion-header-bg-8.jpg') }}" alt="fashion img">
    <div class="aa-catg-head-banner-area">
        <div class="container">
            <div class="aa-catg-head-banner-content">
                <h2>Checkout</h2>
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <!--<li><i class="fa fa-chevron-right breadcrumb-separator"></i></li>  -->       
                    <li><a href="/cart">Cart</a></li>
                    <li class="active">Checkout</li>
                </ol>
            </div>
        </div>
    </div>
</section>  
<!-- / catg header banner section -->


<section id="aa-product-category">
    <!-- notification section -->
    <br>
    <div class="container">
    @if (session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @endif

    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    </div>
    <!-- / notification section -->

</section>

 <!-- Cart view section -->
 <section id="checkout">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="checkout-area">
            <div class="row">   
              <div class="col-md-8">
                <div class="checkout-left">
                  <div class="panel-group" id="accordion">


                    <!-- Login section -->
                    <!--
                    <div class="panel panel-default aa-checkout-login">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                            Client Login 
                          </a>
                        </h4>
                      </div>
                      <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat voluptatibus modi pariatur qui reprehenderit asperiores fugiat deleniti praesentium enim incidunt.</p>
                          <input type="text" placeholder="Username or email">
                          <input type="password" placeholder="Password">
                          <button type="submit" class="aa-browse-btn">Login</button>
                          <label for="rememberme"><input type="checkbox" id="rememberme"> Remember me </label>
                          <p class="aa-lost-password"><a href="#">Lost your password?</a></p>
                        </div>
                      </div>
                    </div>
                    -->
                    <!-- /Login section -->



                    <!-- Billing Details -->
                    <div class="panel panel-default aa-checkout-billaddress">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            Checkout
                          </a>
                        </h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">

                            <form action="{{ route('checkout.store') }}" method="POST" id="payment-form">
                            {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                      <div class="aa-checkout-single-bill">
                                        <label for="name">Full Name</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required placeholder="First Name*">
                                      </div>                             
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                      <div class="aa-checkout-single-bill">
                                        <label for="email">Email Address</label>
                                        @if (auth()->user())
                                        <input type="email" id="email" type="email" name="email" value="{{ auth()->user()->email }}" readonly>
                                        @else
                                        <input type="email" id="email" type="email" name="email" value="{{ old('email') }}" required>
                                        @endif
                                      </div>                             
                                    </div>
                                    <div class="col-md-6">
                                        <div class="aa-checkout-single-bill">
                                            <label for="phone">Phone</label>
                                            <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}" required>
                                        </div>
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-md-12">
                                      <div class="aa-checkout-single-bill">
                                        <label for="address">Address</label>
                                        <input type="text" class="form-control" id="address" name="address" value="{{ old('address') }}" required>
                                      </div>                             
                                    </div>                            
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="aa-checkout-single-bill">
                                            <label for="city">City</label>
                                            <input type="text" class="form-control" id="city" name="city" value="{{ old('city') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="aa-checkout-single-bill">
                                            <label for="province">State/Province</label>
                                            <input type="text" class="form-control" id="province" name="province" value="{{ old('province') }}" required>
                                        </div> 
                                    </div> 
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="aa-checkout-single-bill">
                                            <label for="country">Country</label>
                                            <input type="text" class="form-control" id="country" name="country" value="{{ old('city') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="aa-checkout-single-bill">
                                            <label for="postalcode">Postal Code</label>
                                            <input type="text" class="form-control" id="postalcode" name="postalcode" value="{{ old('postalcode') }}" required>
                                        </div>                            
                                    </div>                            
                                </div>  
                            
                                <!-- stripe -->
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="aa-checkout-single-bill">
                                            <label for="name_on_card">Name on Card</label>
                                            <input type="text" class="form-control" id="name_on_card" name="name_on_card" value="" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id ="creditcardRow">                                
                                    <div class="col-md-12">
                                        <div class="aa-checkout-single-bill">
                                            <label for="card-element">
                                                Credit or debit card
                                            </label>
                                        <div id="card-element">
                                            <!-- a Stripe Element will be inserted here. -->
                                        </div>

                                        <!-- Used to display form errors -->
                                        <div id="card-errors" role="alert"></div>
                                    </div>
                                    <div class="spacer"></div>

                                    <button type="submit" id="complete-order" class="aa-browse-btn">Complete Order</button>

                                    </div>                            
                                </div> 
                                <!-- / stripe -->
                            </form>

                            <!-- Paypal -->
                            @if ($paypalToken) 
                            <!--<form method="post" id="paypal-payment-form" action="{{ route('checkout.paypal') }}">
                            @csrf
                                <div class="row" id ="paypalRow">
                                    <div class="col-md-12">
                                        <div class="aa-checkout-single-bill">
                                            <label for="name_on_card">
                                                Pay with Paypal 
                                                <a href="https://www.paypal.com/mx/cgi-bin/webscr?cmd=xpt/Marketing/general/WIPaypal-outside" class="about_paypal" onclick="javascript:window.open('https://www.paypal.com/mx/cgi-bin/webscr?cmd=xpt/Marketing/general/WIPaypal-outside','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;">
                                                    What is PayPal?
                                                </a>
                                            </label>
                                            
                                            <section>
                                                <div class="bt-drop-in-wrapper">
                                                    <div id="bt-dropin"></div>
                                                </div>  
                                            </section>

                                            <input id="nonce" name="payment_method_nonce" type="hidden" />
                                            <button class="button-primary" type="submit"><span>Pay with PayPal</span></button>
                                        </div>
                                    </div>
                                </div> 
                            </form>-->
                            @endif                     
                            <!-- /Paypal -->

                        </div>
                      </div>
                    </div>
                    <!-- / Billing Details -->



                    <!-- Coupon section -->
                    @if (! session()->has('coupon'))
                    <div class="panel panel-default aa-checkout-coupon">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                            Have a Coupon?
                          </a>
                        </h4>
                      </div>
                      <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <form action="{{ route('coupon.store') }}" method="POST">
                                {{ csrf_field() }}
                                <input class="aa-coupon-code" type="text" placeholder="Coupon Code" name="coupon_code" id="coupon_code">
                                <input class="aa-browse-btn" type="submit" value="Apply Coupon">
                            </form>
                        </div>
                      </div>
                    </div>
                    @endif   
                    <!-- / Coupon section -->

                  </div>
                </div>
              </div>

              <!-- Order Summary -->
              <div class="col-md-4">
                <div class="checkout-right">
                  <h4>Order Summary</h4>
                  <div class="aa-order-summary-area">
                    <table class="table table-responsive table-borderless">
                      <thead>
                        <tr>
                          <th>Product</th>
                          <th>Total</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach (Cart::content() as $item)
                        <tr>
                            <td><img src="{{ productImage($item->model->image) }}" alt="item" class="checkout-table-img">
                            {{ $item->model->name }} <strong> x  {{ $item->qty }}</strong></td>
                            <td>{{ $item->model->presentPrice() }}</td>
                        </tr>
                        @endforeach
                      </tbody>
                      <tfoot>
                        <tr>                          
                          <th>Subtotal</th>
                          <td>{{ presentPrice(Cart::subtotal()) }}</td>
                        </tr>
                        @if (session()->has('coupon'))
                        <tr>                          
                            <th>
                                Discount ({{ session()->get('coupon')['name'] }})
                                &nbsp; 
                                <form action="{{ route('coupon.destroy') }}" method="POST" style="display:block">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                    <button type="submit" style="font-size:14px;">Remove</button>
                                </form>
                            </th>
                            <td>-{{ presentPrice($discount) }}</td>
                        </tr>
                         <tr>                          
                          <th>New Subtotal</th>
                          <td> {{ presentPrice($newSubtotal) }}</td>
                        </tr>
                        @endif
                         <tr>                          
                          <th>Tax</th>
                          <td>{{ presentPrice($newTax) }}</td>
                        </tr>
                         <tr>                          
                          <th>Total</th>
                          <td>{{ presentPrice($newTotal) }}</td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                  <!-- /Order Summary -->

                  <h4>Payment Method</h4>
                  <div class="aa-payment-method">                    
                    <label for="creditcard"><input type="radio" id="creditcard" name="optionsRadios" checked> Credit Card </label>
                    <label for="paypal"><input type="radio" id="paypal" name="optionsRadios" > Via Paypal </label>
                    <img src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_mc_vs_dc_ae.jpg" border="0" alt="PayPal Acceptance Mark">    
                    <!--
                    <input type="submit" value="Place Order" class="aa-browse-btn">                
                    -->
                  </div>
                </div>
              <!-- / Order Summary -->

              </div>
            </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Cart view section -->



@endsection

@section('extra-js')
    <script src="https://js.braintreegateway.com/web/dropin/1.13.0/js/dropin.min.js"></script>

    <script>
        (function(){
            // Create a Stripe client
            var stripe = Stripe('{{ config('services.stripe.key') }}');

            // Create an instance of Elements
            var elements = stripe.elements();

            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
              base: {
                color: '#32325d',
                lineHeight: '18px',
                fontFamily: '"Roboto", Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                  color: '#aab7c4'
                }
              },
              invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
              }
            };

            // Create an instance of the card Element
            var card = elements.create('card', {
                style: style,
                hidePostalCode: true
            });

            // Add an instance of the card Element into the `card-element` <div>
            card.mount('#card-element');

            // Handle real-time validation errors from the card Element.
            card.addEventListener('change', function(event) {
              var displayError = document.getElementById('card-errors');
              if (event.error) {
                displayError.textContent = event.error.message;
              } else {
                displayError.textContent = '';
              }
            });

            // Handle form submission
            var form = document.getElementById('payment-form');
            form.addEventListener('submit', function(event) {
              event.preventDefault();

              // Disable the submit button to prevent repeated clicks
              document.getElementById('complete-order').disabled = true;

              var options = {
                name: document.getElementById('name_on_card').value,
                address_line1: document.getElementById('address').value,
                address_city: document.getElementById('city').value,
                address_state: document.getElementById('province').value,
                address_zip: document.getElementById('postalcode').value
              }

              stripe.createToken(card, options).then(function(result) {
                if (result.error) {
                  // Inform the user if there was an error
                  var errorElement = document.getElementById('card-errors');
                  errorElement.textContent = result.error.message;

                  // Enable the submit button
                  document.getElementById('complete-order').disabled = false;
                } else {
                  // Send the token to your server
                  stripeTokenHandler(result.token);
                }
              });
            });

            function stripeTokenHandler(token) {
              // Insert the token ID into the form so it gets submitted to the server
              var form = document.getElementById('payment-form');
              var hiddenInput = document.createElement('input');
              hiddenInput.setAttribute('type', 'hidden');
              hiddenInput.setAttribute('name', 'stripeToken');
              hiddenInput.setAttribute('value', token.id);
              form.appendChild(hiddenInput);

              // Submit the form
              form.submit();
            }

            // PayPal Stuff
            var form = document.querySelector('#paypal-payment-form');
            var client_token = "{{ $paypalToken }}";

            braintree.dropin.create({
              authorization: client_token,
              selector: '#bt-dropin',
              paypal: {
                flow: 'vault'
              }
            }, function (createErr, instance) {
              if (createErr) {
                console.log('Create Error', createErr);
                return;
              }

              // remove credit card option
              var elem = document.querySelector('.braintree-option__card');
              elem.parentNode.removeChild(elem);

              form.addEventListener('submit', function (event) {
                event.preventDefault();

                instance.requestPaymentMethod(function (err, payload) {
                  if (err) {
                    console.log('Request Payment Method Error', err);
                    return;
                  }

                  // Add the nonce to the form and submit
                  document.querySelector('#nonce').value = payload.nonce;
                  form.submit();
                });
              });
            });

        })();

        $(function(){
            $('input[name="optionsRadios"]').click(function(){
                if ($(this).is(':checked'))
                {
                 //   alert($(this).attr("id"));
                    if ($(this).attr("id") == "paypal")
                    {
                        $("#paypalRow").show();
                        $("#creditcardRow").hide();
                    }
                    else
                    {
                        $("#creditcardRow").show();
                        $("#paypalRow").hide();
                    }
                }
            });
        });
    </script>
@endsection
