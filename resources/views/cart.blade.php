@extends('layout')

@section('title', 'Shopping Cart')

@section('extra-css')
    <link rel="stylesheet" href="{{ asset('css/algolia.css') }}">
@endsection

@section('content')

<!-- catg header banner section -->
<section id="aa-catg-head-banner">
    <img src="{{ asset('img/fashion/fashion-header-bg-8.jpg') }}" alt="fashion img">
    <div class="aa-catg-head-banner-area">
        <div class="container">
            <div class="aa-catg-head-banner-content">
                <h2>Cart Page</h2>
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <!--<li><i class="fa fa-chevron-right breadcrumb-separator"></i></li>  -->       
                    <li class="active">Shopping Cart</li>
                </ol>
            </div>
        </div>
    </div>
</section>  
<!-- / catg header banner section -->


<section id="aa-product-category">
    <!-- notification section -->
    <br>
    <div class="container">
    @if (session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @endif

    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    </div>
    <!-- / notification section -->

</section>


<!-- Cart view section -->
<section id="cart-view">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
            @if (Cart::count() > 0)

            <h2>{{ Cart::count() }} item(s) in Shopping Cart</h2>

            
            <div class="cart-view-area">
              <div class="cart-view-table">
                  <div class="table-responsive">
                     <table class="table">
                       <thead>
                         <tr>
                           <th>Image</th>
                           <th>Product</th>
                           <th>Price</th>
                           <th>Quantity</th>
                           <th>Total</th>
                           <th></th>
                           <th></th>
                         </tr>
                       </thead>
                       <tbody>

                        <!-- Product item row -->
                        @foreach (Cart::content() as $item)
                         <tr>
                           <td>
                            <!-- image data cell-->
                            <a href="{{ route('shop.show', $item->model->slug) }}">
                                <img src="{{ productImage($item->model->image) }}" class="cart-table-img" alt="img">
                            </a>
                            <!-- / image data cell-->
                           </td>
                           <td>
                            <!-- name data cell -->
                            <a class="aa-cart-title" href="{{ route('shop.show', $item->model->slug) }}">
                                {{ $item->model->name }}
                            </a>
                            <!-- / name data cell -->
                           </td>
                           <td>
                            <!-- price data cell -->
                            {{ presentPrice($item->price) }}
                            <!-- /price data cell -->
                           </td>
                           <td>
                            <!-- quantity data cell -->
                            <select class="quantity" data-id="{{ $item->rowId }}" data-productQuantity="{{ $item->model->quantity }}">
                                @for ($i = 1; $i < 5 + 1 ; $i++)
                                    <option {{ $item->qty == $i ? 'selected' : '' }}>{{ $i }}</option>
                                @endfor
                            </select>
                            <!-- / quantity data cell -->
                           </td>
                           <td>
                            <!-- Subtotal data cell-->
                            {{ presentPrice($item->subtotal) }}
                            <!-- / Subtotal data cell-->
                           </td>
                           <td>
                                <!-- Save for later button-->
                                <form action="{{ route('cart.switchToSaveForLater', $item->rowId) }}" method="POST">
                                    {{ csrf_field() }}
                                    <button type="submit" class="aa-cart-view-btn" style="float: none;">Save for Later</button>
                                </form>
                                <!-- / Save for later button-->
                            </td>
                            <td>
                                <!-- delete data cell -->
                                <form action="{{ route('cart.destroy', $item->rowId) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                    <button type="submit" class="aa-cart-view-btn" style="float: none;">Delete</button>
                                </form>
                                <!-- / delete data cell -->
                             </td>
                         </tr>
                        @endforeach
                        <!-- / Product item row -->
                        @if (! session()->has('coupon'))
                        <tr>
                            <td colspan="7" class="aa-cart-view-bottom">
                                <div class="aa-cart-coupon">
                                    <form action="{{ route('coupon.store') }}" method="POST">
                                        {{ csrf_field() }}
                                        <input class="aa-coupon-code" type="text" placeholder="Coupon" name="coupon_code" id="coupon_code">
                                        <input class="aa-cart-view-btn" type="submit" value="Apply Coupon">
                                    </form>
                                </div>
                                <input class="aa-cart-view-btn" type="submit" value="Update Cart">
                            </td>
                        </tr> 
                        @endif           

                         </tbody>
                     </table>
                   </div>
                <!-- Cart Total view -->
                <div class="cart-view-total">
                  <h4>Cart Totals</h4>
                  <table class="aa-totals-table">
                    <tbody>
                        <tr>
                            <th>Subtotal</th>
                            <td>{{ presentPrice(Cart::subtotal()) }}</td>
                        </tr>
                        @if (session()->has('coupon'))
                        <tr>
                            <th>
                                Coupon ({{ session()->get('coupon')['name'] }})
                                <form action="{{ route('coupon.destroy') }}" method="POST" style="display:block">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                    <button type="submit" style="font-size:14px;">Remove</button>
                                </form>
                            </th>
                            <td>-{{ presentPrice($discount) }} </td>
                        </tr>
                        <tr>
                            <th>New Subtotal</th>
                            <td>{{ presentPrice($newSubtotal) }}</td>
                        </tr>
                        @endif
                        <tr>
                            <th>Tax ({{config('cart.tax')}}%)</th>
                            <td>{{ presentPrice($newTax) }}</td>
                        </tr>
                        <tr>
                            <th>Total</th>
                            <td>{{ presentPrice($newTotal) }}</td>
                        </tr>
                    </tbody>
                  </table>
                <a href="{{ route('checkout.index') }}"  class="aa-cart-view-btn">Proceed to Checkout</a>
                <a href="{{ route('shop.index') }}"  class="aa-cart-view-btn">Continue Shopping</a>
                </div>
              </div>
            </div>

            @else

                <h3>No items in Cart!</h3>
                <div class="spacer"></div>
                <a href="{{ route('shop.index') }}" class="button">Continue Shopping</a>
                <div class="spacer"></div>

            @endif
        
      </div>
    </div>
  </div>
</section>
<!-- / Cart view section -->


<!-- Save for Later view section -->
<section id="cart-view">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
            @if (Cart::instance('saveForLater')->count() > 0)

            <h2>{{ Cart::instance('saveForLater')->count() }} item(s) Saved For Later</h2>

            
            <div class="cart-view-area">
              <div class="cart-view-table">
                  <div class="table-responsive">
                     <table class="table">
                       <thead>
                         <tr>
                           <th>Image</th>
                           <th>Product</th>
                           <th>Price</th>
                           <th></th>
                           <th></th>
                         </tr>
                       </thead>
                       <tbody>

                        <!-- Product item row -->
                        @foreach (Cart::instance('saveForLater')->content() as $item)
                         <tr>
                           <td>
                            <!-- image data cell-->
                            <a href="{{ route('shop.show', $item->model->slug) }}">
                                <img src="{{ productImage($item->model->image) }}" class="cart-table-img" alt="img">
                            </a>
                            <!-- / image data cell-->
                           </td>
                           <td>
                            <!-- name data cell -->
                            <a class="aa-cart-title" href="{{ route('shop.show', $item->model->slug) }}">
                                {{ $item->model->name }}
                            </a>
                            <!-- / name data cell -->
                           </td>
                           <td>
                            <!-- price data cell -->
                            {{ $item->model->presentPrice() }}
                            <!-- /price data cell -->
                           </td>
                            <td>
                                <!-- Move to Cart button-->
                                <form action="{{ route('saveForLater.switchToCart', $item->rowId) }}" method="POST">
                                    {{ csrf_field() }}

                                    <button type="submit" class="aa-cart-view-btn" style="float: none;">Move to Cart</button>
                                </form>
                                <!-- / Move to Cart button-->
                            </td>
                            <td>
                                <!-- delete data cell -->
                                <form action="{{ route('saveForLater.destroy', $item->rowId) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                    <button type="submit" class="aa-cart-view-btn" style="float: none;">Remove</button>
                                </form>
                                <!-- / delete data cell -->
                             </td>
                         </tr>
                        @endforeach
                        <!-- / Product item row -->
                         </tbody>
                     </table>
                   </div>
                </div>
              </div>
            </div>

            @else

                <h3>You have no items Saved for Later.</h3>
                <br>

            @endif
        
        <!-- Related Products Section -->
        @include('partials.might-like')
        <!-- / Related Products Section -->
      </div>
    </div>
  </div>
</section>
<!-- / Save for Later view section -->



@endsection

@section('extra-js')
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        (function(){
            const classname = document.querySelectorAll('.quantity')

            Array.from(classname).forEach(function(element) {
                element.addEventListener('change', function() {
                    const id = element.getAttribute('data-id')
                    const productQuantity = element.getAttribute('data-productQuantity')

                    axios.patch(`/cart/${id}`, {
                        quantity: this.value,
                        productQuantity: productQuantity
                    })
                    .then(function (response) {
                        // console.log(response);
                        window.location.href = '{{ route('cart.index') }}'
                    })
                    .catch(function (error) {
                        // console.log(error);
                        window.location.href = '{{ route('cart.index') }}'
                    });
                })
            })
        })();
    </script>

    <!-- Include AlgoliaSearch JS Client and autocomplete.js library -->
    <script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
    <script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.min.js"></script>
    <script src="{{ asset('js/algolia.js') }}"></script>
@endsection
