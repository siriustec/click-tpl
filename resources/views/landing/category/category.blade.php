 @foreach ($products as $product)
 <li>
    <figure>
      <a class="aa-product-img" href="{{ route('shop.show', $product->slug) }}"><img src="{{ productImage($product->image) }}" alt="polo shirt img"></a>
      <a class="aa-add-card-btn"href="#"><span class="fa fa-shopping-cart"></span>Add To Cart</a>
       <figcaption>
        <h4 class="aa-product-title"><a href="#">Polo T-Shirt</a></h4>
        <span class="aa-product-price">{{ $product->presentPrice() }}</span><span class="aa-product-price"><del>{{ $product->presentPrice() }}</del></span>
      </figcaption>
    </figure>                     
    <div class="aa-product-hvr-content">
      <a href="#" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="fa fa-heart-o"></span></a>
      <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><span class="fa fa-exchange"></span></a>
      <a href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal"><span class="fa fa-search"></span></a>                            
    </div>
    <!-- product badge -->
        <span class="aa-badge aa-hot" href="#">HOT!</span>
  </li>
  @endforeach                              


