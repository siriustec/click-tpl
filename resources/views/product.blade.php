@extends('layout')

@section('title', $product->name)

@section('extra-css')
    <link rel="stylesheet" href="{{ asset('css/algolia.css') }}">
@endsection

@section('content')

  <!-- catg header banner section -->
    <section id="aa-catg-head-banner">
        <img src="{{ asset('img/fashion/fashion-header-bg-8.jpg') }}" alt="fashion img">
        <div class="aa-catg-head-banner-area">
            <div class="container">
                <div class="aa-catg-head-banner-content">
                    <h2>{{ $product->name }}</h2>
                    <ol class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <!--<li><i class="fa fa-chevron-right breadcrumb-separator"></i></li>  -->       
                        <li><a href="{{ route('shop.index') }}">Shop</a></li>
                        <li class="active">{{ $product->name }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>  
  <!-- / catg header banner section -->


    <section id="aa-product-category">
        <!-- notification section -->
        <br>
        <div class="container">
        @if (session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @endif

        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        </div>
        <!-- / notification section -->

    </section>


    <!-- main product section -->
    <section id="aa-product-details">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aa-product-details-area">
                        <!-- Product Main Details Area -->
                        <div class="aa-product-details-content">
                            <div class="row">
                                <!-- Modal view slider -->
                                <div class="col-md-5 col-sm-5 col-xs-12">
                                    <div class="aa-product-view-slider">
                                        <div id="demo-1" class="simpleLens-gallery-container">
                                            <div class="simpleLens-container">
                                                <div class="simpleLens-big-image-container">
                                                    <a data-lens-image="{{ productImage($product->image) }}" class="simpleLens-lens-image">
                                                        <img src="{{ productImage($product->image) }}" class="simpleLens-big-image">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="simpleLens-thumbnails-container">
                                            @if ($product->images)
                                                <a data-big-image="{{ productImage($product->image) }}" data-lens-image="{{ productImage($product->image) }}" class="simpleLens-thumbnail-wrapper" >
                                                    <!--<div class="product-section-thumbnail">
                                                    </div>-->
                                                        <img class="product-section-thumbnail" src="{{ productImage($product->image) }}" alt="product">
                                                </a>     
                                                @foreach (json_decode($product->images, true) as $image)
                                                <a data-big-image="{{ productImage($image) }}" data-lens-image="{{ productImage($image) }}" class="simpleLens-thumbnail-wrapper" >
                                                    <!--<div class="product-section-thumbnail">
                                                    </div>-->
                                                        <img class="product-section-thumbnail" src="{{ productImage($image) }}" alt="product">
                                                </a>     
                                                @endforeach
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Modal view content -->
                                <div class="col-md-7 col-sm-7 col-xs-12">
                                    <div class="aa-product-view-content">
                                        <h3>{{ $product->name }}</h3>
                                        <div class="aa-price-block">
                                            <span class="aa-product-view-price">{{ $product->presentPrice() }}</span>
                                            <p class="aa-product-avilability">Availability: <span>{!! $stockLevel !!}</span></p>
                                        </div>
                                        <p>{{ $product->details }}</p>
                                        <div class="aa-prod-quantity">
                                            <p class="aa-prod-category">
                                                Category: <a href="#">{{ $product->presentPrice() }}</a>
                                            </p>
                                        </div>
                                        <div class="aa-prod-view-bottom">
                                            @if ($product->quantity > 0)
                                                <form action="{{ route('cart.store', $product) }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="aa-add-to-cart-btn">Add to Cart</button>
                                                </form>
                                            @endif
                                            <!-- <a class="aa-add-to-cart-btn" href="#">Wishlist</a> -->
                                            <!-- <a class="aa-add-to-cart-btn" href="#">Compare</a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Product Main Details Area -->

                        <!-- Product Details Section -->
                        <div class="aa-product-details-bottom">
                            <ul class="nav nav-tabs" id="myTab2">
                                <li><a href="#description" data-toggle="tab">Description</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="description">
                                    <p>
                                        {!! $product->description !!}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- Product Details Section -->
                        
                        <!-- Related Products Section -->
                        @include('partials.might-like')
                        <!-- / Related Products Section -->
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- / main product section -->



@endsection

@section('extra-js')
    <script>
        (function(){
            const currentImage = document.querySelector('#currentImage');
            const images = document.querySelectorAll('.product-section-thumbnail');

            images.forEach((element) => element.addEventListener('click', thumbnailClick));

            function thumbnailClick(e) {
                currentImage.classList.remove('active');

                currentImage.addEventListener('transitionend', () => {
                    currentImage.src = this.querySelector('img').src;
                    currentImage.classList.add('active');
                })

                images.forEach((element) => element.classList.remove('selected'));
                this.classList.add('selected');
            }

        })();
    </script>

    <!-- Include AlgoliaSearch JS Client and autocomplete.js library -->
    <script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
    <script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.min.js"></script>
    <script src="{{ asset('js/algolia.js') }}"></script>

@endsection
