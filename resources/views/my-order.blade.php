@extends('layout')

@section('title', 'My Order')

@section('extra-css')
    <link rel="stylesheet" href="{{ asset('css/algolia.css') }}">
@endsection

@section('content')

<!-- catg header banner section -->
<section id="aa-catg-head-banner">
    <img src="{{ asset('img/fashion/fashion-header-bg-8.jpg') }}" alt="fashion img">
    <div class="aa-catg-head-banner-area">
        <div class="container">
            <div class="aa-catg-head-banner-content">
                <h2>My Order</h2>
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <!--<li><i class="fa fa-chevron-right breadcrumb-separator"></i></li>  -->       
                    <li class="">My Orders</li>
                    <li class="active">My Order</li>
                </ol>
            </div>
        </div>
    </div>
</section>  
<!-- / catg header banner section -->

<section id="aa-product-category">
    <!-- notification section -->
    <br>
    <div class="container">
    @if (session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @endif

    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    </div>
    <!-- / notification section -->
</section>


 <!-- Login View section -->
 <section id="aa-myaccount">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="aa-myaccount-area">         
            <div class="row">

              <!-- Login further links -->
                <div class="col-lg-9 col-md-9 col-sm-8 col-md-push-3">

                    <div class="products-section my-orders container">
                        <div class="my-profile">
                            <div class="products-header">
                                <h1 class="stylish-heading">Order ID: {{ $order->id }}</h1>
                            </div>

                            <div style="width: 41.5vh;;">
                                <div class="order-container">
                                    <div class="order-header">
                                        <div class="order-header-items">
                                            <div>
                                                <div class="uppercase font-bold">Order Placed</div>
                                                <div>{{ presentDate($order->created_at) }}</div>
                                            </div>
                                            <div>
                                                <div class="uppercase font-bold">Order ID</div>
                                                <div>{{ $order->id }}</div>
                                            </div><div>
                                                <div class="uppercase font-bold">Total</div>
                                                <div>{{ presentPrice($order->billing_total) }}</div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="order-header-items">
                                                <div><a href="#">Invoice</a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="order-products">
                                        <table class="table" style="">
                                            <tbody>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>{{ $order->user->name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Address</td>
                                                    <td>{{ $order->billing_address }}</td>
                                                </tr>
                                                <tr>
                                                    <td>City</td>
                                                    <td>{{ $order->billing_city }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Subtotal</td>
                                                    <td>{{ presentPrice($order->billing_subtotal) }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tax</td>
                                                    <td>{{ presentPrice($order->billing_tax) }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Total</td>
                                                    <td>{{ presentPrice($order->billing_total) }}</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div> <!-- end order-container -->

                                <div class="order-container">
                                    <div class="order-header">
                                        <div class="order-header-items">
                                            <div>
                                                Order Items
                                            </div>

                                        </div>
                                    </div>
                                    <div class="order-products">
                                        @foreach ($products as $product)
                                            <div class="order-product-item">
                                                <div><img src="{{  productImage($product->image)  }}" alt="Product Image"></div>
                                                <div>
                                                    <div>
                                                        <a href="{{ route('shop.show', $product->slug) }}">{{ $product->name }}</a>
                                                    </div>
                                                    <div>{{ presentPrice($product->price) }}</div>
                                                    <div>Quantity: {{ $product->pivot->quantity }}</div>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                </div> <!-- end order-container -->
                            </div>

                            <div class="spacer"></div>
                        </div>
                    </div>


              </div>
              <!-- / Login further links -->
              <!-- Login further links -->
              <div class="col-lg-3 col-md-3 col-sm-4 col-md-pull-9">
                    <aside class="aa-sidebar">
                        <!-- single sidebar -->
                        <div class="aa-sidebar-widget">
                            <h3>Profile</h3>
                            <ul class="aa-catg-nav">
                                <li>
                                    <a href="{{ route('users.edit') }}">
                                        My Profile
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="{{ route('orders.index') }}" >
                                        My Orders
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </aside>
                </div>
              <!-- / Login further links -->

            </div>          
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Login View section -->




@endsection

@section('extra-js')
    <!-- Include AlgoliaSearch JS Client and autocomplete.js library -->
    <script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
    <script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.min.js"></script>
    <script src="{{ asset('js/algolia.js') }}"></script>
@endsection
