@extends('layout')

@section('title', 'Reset Password')

@section('extra-css')
    <link rel="stylesheet" href="{{ asset('css/algolia.css') }}">
@endsection

@section('content')

<!-- catg header banner section -->
<section id="aa-catg-head-banner">
    <img src="{{ asset('img/fashion/fashion-header-bg-8.jpg') }}" alt="fashion img">
    <div class="aa-catg-head-banner-area">
        <div class="container">
            <div class="aa-catg-head-banner-content">
                <h2>Login</h2>
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <!--<li><i class="fa fa-chevron-right breadcrumb-separator"></i></li>  -->       
                    <li class="active">Login</li>
                </ol>
            </div>
        </div>
    </div>
</section>  
<!-- / catg header banner section -->


<section id="aa-product-category">
    <!-- notification section -->
    <br>
    <div class="container">
    @if (session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @endif

    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    </div>
    <!-- / notification section -->

</section>

 <!-- Login View section -->
 <section id="aa-myaccount">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="aa-myaccount-area">         
            <div class="row">
              <div class="col-md-6">
                <div class="aa-myaccount-login">
                <h4>Forgot Password?</h4>

                <form action="{{ route('password.email') }}" method="POST" class="aa-login-form">
                    {{ csrf_field() }}
                    
                    <input type="text" id="email" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>

                    <button type="submit" class="aa-browse-btn">Send Password Reset Link</button>
                    
                  </form>
                </div>
              </div>

              <!-- Login further links -->
              <div class="col-md-6">
                <div class="aa-myaccount-register">
                    <h4>Forgotten Password Information</h4>

                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel dicta obcaecati exercitationem ut atque inventore cum. Magni autem error ut!</p>

                    <div class="spacer"></div>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vel accusantium quasi necessitatibus rerum fugiat eos, a repudiandae tempore nisi ipsa delectus sunt natus!</p>
                    
                </div>
              </div>
              <!-- / Login further links -->
            </div>          
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Login View section -->

@endsection
