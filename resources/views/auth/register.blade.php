@extends('layout')

@section('title', 'Sign Up for an Account')

@section('extra-css')
    <link rel="stylesheet" href="{{ asset('css/algolia.css') }}">
@endsection

@section('content')

<!-- catg header banner section -->
<section id="aa-catg-head-banner">
    <img src="{{ asset('img/fashion/fashion-header-bg-8.jpg') }}" alt="fashion img">
    <div class="aa-catg-head-banner-area">
        <div class="container">
            <div class="aa-catg-head-banner-content">
                <h2>Register</h2>
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <!--<li><i class="fa fa-chevron-right breadcrumb-separator"></i></li>  -->       
                    <li class="active">Register</li>
                </ol>
            </div>
        </div>
    </div>
</section>  
<!-- / catg header banner section -->


<section id="aa-product-category">
    <!-- notification section -->
    <br>
    <div class="container">
    @if (session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @endif

    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    </div>
    <!-- / notification section -->

</section>

 <!-- Login View section -->
 <section id="aa-myaccount">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="aa-myaccount-area">         
            <div class="row">
              <div class="col-md-6">
                <div class="aa-myaccount-login">
                <h4>Create Account</h4>

                <form action="{{ route('register') }}" method="POST" class="aa-login-form">
                    {{ csrf_field() }}

                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>

                    <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required>

                    <input id="password" type="password" class="form-control" name="password" placeholder="Password" placeholder="Password" required>

                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>


                    <button type="submit" class="aa-browse-btn">Create Account</button>
                    <br>
                    <br>
                    <br>
                    <p class="aa-lost-password">
                        <strong>Already have an account?</strong>
                        <br>
                        <a href="{{ route('login') }}">Login</a>
                    </p>
                  </form>
                </div>
              </div>

              <!-- Login further links -->
              <div class="col-md-6">
                <div class="aa-myaccount-register">
                    <h4>New Customer</h4>

                    <div class="spacer"></div>
                    <p><strong>Save time now.</strong></p>
                    <p>Creating an account will allow you to checkout faster in the future, have easy access to order history and customize your experience to suit your preferences.</p>

                    &nbsp;
                    
                    <div class="spacer"></div>
                    <p><strong>Loyalty Program</strong></p>
                    <p><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt debitis, amet magnam accusamus nisi distinctio eveniet ullam. Facere, cumque architecto.</p></p>

                </div>
              </div>
              <!-- / Login further links -->
            </div>          
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Login View section -->

@endsection