@extends('layout')

@section('title', 'Login')

@section('extra-css')
    <link rel="stylesheet" href="{{ asset('css/algolia.css') }}">
@endsection

@section('content')

<!-- catg header banner section -->
<section id="aa-catg-head-banner">
    <img src="{{ asset('img/fashion/fashion-header-bg-8.jpg') }}" alt="fashion img">
    <div class="aa-catg-head-banner-area">
        <div class="container">
            <div class="aa-catg-head-banner-content">
                <h2>Login</h2>
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <!--<li><i class="fa fa-chevron-right breadcrumb-separator"></i></li>  -->       
                    <li class="active">Login</li>
                </ol>
            </div>
        </div>
    </div>
</section>  
<!-- / catg header banner section -->


<section id="aa-product-category">
    <!-- notification section -->
    <br>
    <div class="container">
    @if (session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @endif

    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    </div>
    <!-- / notification section -->

</section>

 <!-- Login View section -->
 <section id="aa-myaccount">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="aa-myaccount-area">         
            <div class="row">
              <div class="col-md-6">
                <div class="aa-myaccount-login">
                <h4>Returning Customer</h4>

                <form action="{{ route('login') }}" method="POST" class="aa-login-form">
                    {{ csrf_field() }}
                    <label for="">Username or Email address<span>*</span></label>
                    <input type="text" id="email" name="email" value="{{ old('email') }}" placeholder="Email" required>


                    <label for="">Password<span>*</span></label>
                    <input type="password" id="password" name="password" value="{{ old('password') }}" placeholder="Password" required>

                    <button type="submit" class="aa-browse-btn">Login</button>
                    <label class="rememberme" for="rememberme">
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                    </label>

                    <p class="aa-lost-password">
                        <a href="{{ route('password.request') }}">
                            Lost your password?
                        </a>
                    </p>
                  </form>
                </div>
              </div>

              <!-- Login further links -->
              <div class="col-md-6">
                <div class="aa-myaccount-register">                 
                    <h4>New Customer</h4>

                    <div class="spacer"></div>
                    <p><strong>Save time now.</strong></p>
                    <p>You don't need an account to checkout.</p>

                    <div class="spacer"></div>
                    <p class="aa-lost-password">
                        <a href="{{ route('guestCheckout.index') }}" class="aa-browse-btn">
                            Continue as Guest
                        </a>
                    </p>
                    
                    <div class="spacer"></div>
                    &nbsp;
                    
                    <div class="spacer"></div>
                    <p><strong>Save time later.</strong></p>
                    <p>Create an account for fast checkout and easy access to order history.</p>

                    <div class="spacer"></div>
                    <p class="aa-lost-password">
                        <a href="{{ route('register') }}" class="aa-browse-btn">
                            Create Account
                        </a>
                    </p>
                    
                </div>
              </div>
              <!-- / Login further links -->
            </div>          
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Login View section -->

@endsection
