@extends('layout')

@section('title', 'Products')
@section('body-class', 'productPage')

@section('extra-css')
	<link rel="stylesheet" href="{{ asset('css/algolia.css') }}">
@endsection

@section('content')



  <!-- catg header banner section -->
	<section id="aa-catg-head-banner">
		<img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
		<div class="aa-catg-head-banner-area">
			<div class="container">
				<div class="aa-catg-head-banner-content">
					<h2>{{ $categoryName }}</h2>
					<ol class="breadcrumb">
						<li><a href="/">Home</a></li>
						<!--<li><i class="fa fa-chevron-right breadcrumb-separator"></i></li>  -->       
						<li class="active">Shop</li>
					</ol>
				</div>
			</div>
		</div>
	</section>	
  <!-- / catg header banner section -->


	<section id="aa-product-category">
  		<!-- notification section -->
		<br>
		<div class="container">
		@if (session()->has('success_message'))
			<div class="alert alert-success">
				{{ session()->get('success_message') }}
			</div>
		@endif

		@if(count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		</div>
  		<!-- / notification section -->

		<div class="container">
			<div class="row">
				<!-- Product Catalog Container -->
				<div class="col-lg-9 col-md-9 col-sm-8 col-md-push-3">
					<div class="aa-product-catg-content">
						<!-- Product Catalog Header -->
						<div class="aa-product-catg-head">
							<div class="aa-product-catg-head-left">
								<label for="">Sort by Price:</label>
								<a href="{{ route('shop.index', ['category'=> request()->category, 'sort' => 'low_high']) }}">Low to High</a> |
								<a href="{{ route('shop.index', ['category'=> request()->category, 'sort' => 'high_low']) }}">High to Low</a>
								<!--
								<form action="" class="aa-sort-form">
									<label for="">Sort by Price:</label>
									<a href="{{ route('shop.index', ['category'=> request()->category, 'sort' => 'low_high']) }}">Low to High</a> |
									<a href="{{ route('shop.index', ['category'=> request()->category, 'sort' => 'high_low']) }}">High to Low</a>
									<select name="">
									<option value="1" selected="Default">Default</option>
									<option value="2">Name</option>
									<option value="3">Price</option>
									<option value="4">Date</option>
									</select>
								</form>
								<form action="" class="aa-show-form">
									<label for="">Show</label>
									<select name="">
									<option value="1" selected="12">12</option>
									<option value="2">24</option>
									<option value="3">36</option>
									</select>
								</form>
							-->
							</div>
							<div class="aa-product-catg-head-right">
							<!--<a id="grid-catg" href="#"><span class="fa fa-th"></span></a> -->
							<!--<a id="list-catg" href="#"><span class="fa fa-list"></span></a> -->

							</div>
						</div>
						<!-- / Product Catalog Header -->

						<!-- Product Catalog List -->
						<div class="aa-product-catg-body">
							<ul class="aa-product-catg">
							<!-- product item -->
								@forelse ($products as $product)
								<li>
									<figure>
										<a class="aa-product-img" href="{{ route('shop.show', $product->slug) }}">
											<img class="img-item" src="{{ productImage($product->image) }}" alt="product">
										</a>
						                 @if ($product->quantity > 0)
						                <form action="{{ route('cart.store', $product) }}" id="addToCart_{{ $product->slug}}" method="POST">
						                    {{ csrf_field() }}
											<a class="aa-add-card-btn" href="#"  onclick="document.getElementById('addToCart_{{ $product->slug}}').submit();">
												<span class="fa fa-shopping-cart"></span>Add To Cart
											</a>						                    
						                </form>
						                @endif
										<figcaption>
											<h4 class="aa-product-title">
												<a href="{{ route('shop.show', $product->slug) }}">
													{{ $product->name }}
												</a>
											</h4>										
											<span class="aa-product-price">
												{{ $product->presentPrice() }}
											</span>
											<span class="aa-product-price">
												<del>{{ $product->presentPrice() }}</del>
											</span>
											<p class="aa-product-descrip">
												{{ $product->description }}
											</p>
										</figcaption>
									</figure>                         
									<div class="aa-product-hvr-content">
										<!--<a href="#" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="fa fa-heart-o"></span></a> -->
										<!--<a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><span class="fa fa-exchange"></span></a> -->
										<!--<a href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal"><span class="fa fa-search"></span></a> -->
									</div>
									<!-- product badge -->
									<span class="aa-badge aa-sale" href="#">SALE!</span>
								</li>
								@empty
								<div class="aa-product-related-item">
									<h3 style="text-align: left">No items found</h3>
								</div>
								@endforelse
							<!-- / product item -->
							</ul>

							<!-- quick view modal -->                  
							<div class="modal fade" id="quick-view-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<div class="row">
												<!-- Modal view slider -->
												<div class="col-md-6 col-sm-6 col-xs-12">
													<div class="aa-product-view-slider">
														<div class="simpleLens-gallery-container" id="demo-1">
															<div class="simpleLens-container">
																<div class="simpleLens-big-image-container">
																	<a class="simpleLens-lens-image" data-lens-image="img/view-slider/large/polo-shirt-1.png">
																	<img src="img/view-slider/medium/polo-shirt-1.png" class="simpleLens-big-image">
																	</a>
																</div>
															</div>
															<div class="simpleLens-thumbnails-container">
																<a href="#" class="simpleLens-thumbnail-wrapper"
																	data-lens-image="img/view-slider/large/polo-shirt-1.png"
																	data-big-image="img/view-slider/medium/polo-shirt-1.png">
																<img src="img/view-slider/thumbnail/polo-shirt-1.png">
																</a>                                    
																<a href="#" class="simpleLens-thumbnail-wrapper"
																	data-lens-image="img/view-slider/large/polo-shirt-3.png"
																	data-big-image="img/view-slider/medium/polo-shirt-3.png">
																<img src="img/view-slider/thumbnail/polo-shirt-3.png">
																</a>
																<a href="#" class="simpleLens-thumbnail-wrapper"
																	data-lens-image="img/view-slider/large/polo-shirt-4.png"
																	data-big-image="img/view-slider/medium/polo-shirt-4.png">
																<img src="img/view-slider/thumbnail/polo-shirt-4.png">
																</a>
															</div>
														</div>
													</div>
												</div>
												<!-- Modal view content -->
												<div class="col-md-6 col-sm-6 col-xs-12">
													<div class="aa-product-view-content">
														<h3>T-Shirt</h3>
														<div class="aa-price-block">
															<span class="aa-product-view-price">$34.99</span>
															<p class="aa-product-avilability">Availability: <span>In stock</span></p>
														</div>
														<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis animi, veritatis quae repudiandae quod nulla porro quidem, itaque quis quaerat!</p>
														<h4>Size</h4>
														<div class="aa-prod-view-size">
															<a href="#">S</a>
															<a href="#">M</a>
															<a href="#">L</a>
															<a href="#">XL</a>
														</div>
														<div class="aa-prod-quantity">
															<form action="">
																<select name="" id="">
																	<option value="0" selected="1">1</option>
																	<option value="1">2</option>
																	<option value="2">3</option>
																	<option value="3">4</option>
																	<option value="4">5</option>
																	<option value="5">6</option>
																</select>
															</form>
															<p class="aa-prod-category">
																Category: <a href="#">Polo T-Shirt</a>
															</p>
														</div>
														<div class="aa-prod-view-bottom">
															<a href="#" class="aa-add-to-cart-btn"><span class="fa fa-shopping-cart"></span>Add To Cart</a>
															<a href="#" class="aa-add-to-cart-btn">View Details</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							<!-- / quick view modal -->			  
						</div>
						<!-- / Product Catalog List -->

						<!-- Product Pagination -->
						<div class="aa-product-catg-pagination">
							{{ $products->appends(request()->input())->links() }}
						</div>
						<!-- / Product Pagination -->
					</div>
				</div>
				<!-- / Product Catalog Container -->
				
				<!-- Product side bar -->
        		<div class="col-lg-3 col-md-3 col-sm-4 col-md-pull-9">
        			<aside class="aa-sidebar">
						<!-- single sidebar -->
						<div class="aa-sidebar-widget">
							<h3>Category</h3>
							<ul class="aa-catg-nav">
							@foreach ($categories as $category)
								<li class="{{ setActiveCategory($category->slug) }}"><a href="{{ route('shop.index', ['category' => $category->slug]) }}">{{ $category->name }}</a></li>
							@endforeach
							</ul>
						</div>
						<!-- / single sidebar -->
						<!-- Tags sidebar --> <!--
						<div class="aa-sidebar-widget">
							<h3>Tags</h3>
							<div class="tag-cloud">
								<a href="#">Fashion</a>
								<a href="#">Ecommerce</a>
								<a href="#">Shop</a>
								<a href="#">Hand Bag</a>
								<a href="#">Laptop</a>
								<a href="#">Head Phone</a>
								<a href="#">Pen Drive</a>
							</div>
						</div> -->
						<!-- / Tags sidebar -->
						<!-- Shop By Price sidebar --><!--
						<div class="aa-sidebar-widget">
							<h3>Shop By Price</h3>
							<div class="aa-sidebar-price-range">
								<form action="">
									<div id="skipstep" class="noUi-target noUi-ltr noUi-horizontal noUi-background">
									</div>
									<span id="skip-value-lower" class="example-val">30.00</span>
									<span id="skip-value-upper" class="example-val">100.00</span>
									<button class="aa-filter-btn" type="submit">Filter</button>
								</form>
							</div>
						</div> -->
						<!-- / Shop By Price sidebar -->
						<!-- Shop By Color sidebar --><!--
						<div class="aa-sidebar-widget">
							<h3>Shop By Color</h3>
							<div class="aa-color-tag">
								<a class="aa-color-green" href="#"></a>
								<a class="aa-color-yellow" href="#"></a>
								<a class="aa-color-pink" href="#"></a>
								<a class="aa-color-purple" href="#"></a>
								<a class="aa-color-blue" href="#"></a>
								<a class="aa-color-orange" href="#"></a>
								<a class="aa-color-gray" href="#"></a>
								<a class="aa-color-black" href="#"></a>
								<a class="aa-color-white" href="#"></a>
								<a class="aa-color-cyan" href="#"></a>
								<a class="aa-color-olive" href="#"></a>
								<a class="aa-color-orchid" href="#"></a>
							</div>
						</div> -->
						<!-- / Shop By Color sidebar -->
						<!-- Recently Views sidebar --><!--
						<div class="aa-sidebar-widget">
							<h3>Recently Views</h3>
							<div class="aa-recently-views">
								<ul>
									<li>
										<a href="#" class="aa-cartbox-img"><img alt="img" src="img/woman-small-2.jpg"></a>
										<div class="aa-cartbox-info">
											<h4><a href="#">Product Name</a></h4>
											<p>1 x $250</p>
										</div>
									</li>
									<li>
										<a href="#" class="aa-cartbox-img"><img alt="img" src="img/woman-small-1.jpg"></a>
										<div class="aa-cartbox-info">
											<h4><a href="#">Product Name</a></h4>
											<p>1 x $250</p>
										</div>
									</li>
									<li>
										<a href="#" class="aa-cartbox-img"><img alt="img" src="img/woman-small-2.jpg"></a>
										<div class="aa-cartbox-info">
											<h4><a href="#">Product Name</a></h4>
											<p>1 x $250</p>
										</div>
									</li>
								</ul>
							</div>
						</div> -->
						<!-- / Recently Views sidebar -->
						<!-- Top Rated Products sidebar --><!--
						<div class="aa-sidebar-widget">
							<h3>Top Rated Products</h3>
							<div class="aa-recently-views">
								<ul>
									<li>
										<a href="#" class="aa-cartbox-img"><img alt="img" src="img/woman-small-2.jpg"></a>
										<div class="aa-cartbox-info">
											<h4><a href="#">Product Name</a></h4>
											<p>1 x $250</p>
										</div>
									</li>
									<li>
										<a href="#" class="aa-cartbox-img"><img alt="img" src="img/woman-small-1.jpg"></a>
										<div class="aa-cartbox-info">
											<h4><a href="#">Product Name</a></h4>
											<p>1 x $250</p>
										</div>
									</li>
									<li>
										<a href="#" class="aa-cartbox-img"><img alt="img" src="img/woman-small-2.jpg"></a>
										<div class="aa-cartbox-info">
											<h4><a href="#">Product Name</a></h4>
											<p>1 x $250</p>
										</div>
									</li>
								</ul>
							</div>
						</div> -->
						<!-- / Top Rated Products sidebar -->

					</aside>
        		</div>
				<!-- Product side bar -->

			</div>
		</div>
	</section>


@endsection

@section('extra-js')
	<!-- Include AlgoliaSearch JS Client and autocomplete.js library -->
	<script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
	<script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.min.js"></script>
	<script src="{{ asset('js/algolia.js') }}"></script>
@endsection
